import io
import json
import subprocess
import tempfile


def update_database():
    print("trivy: Update Database")
    command_list = ["trivy", "image", "--download-db-only"]
    result = subprocess.run(command_list)
    print("trivy: Stdout: ", result.stdout)
    print("trivy: Stderr: ", result.stderr)
    print("trivy: RC: ", result.returncode)


def scan(image, uid):
    print("trivy: Scan")
    print("trivy: Image: ", image)
    filepath = "/tmp/" + uid
    command_list = [
        "trivy",
        "image",
        "--skip-update",
        "--no-progress",
        "-o",
        filepath,
        "-f",
        "json",
        image,
    ]
    result = subprocess.run(command_list)
    print("trivy: Stdout: ", result.stdout)
    print("trivy: Stderr: ", result.stderr)
    print("trivy: RC: ", result.returncode)
    if not result.returncode == 0:
        raise Exception(result.stderr)

    with io.open(filepath) as f:
        data = json.load(f)

    return data


def clear_cache():
    print("trivy: Clear Cache")
    command_list = ["trivy", "image", "--clear-cache"]
    result = subprocess.run(command_list)
    print("trivy: Stdout: ", result.stdout)
    print("trivy: Stderr: ", result.stderr)
    print("trivy: RC: ", result.returncode)


"""
func (c *TrivyClient) Do(ctx context.Context, image string) ([]byte, error) {

	tmpfile, err := ioutil.TempFile("", "*.json")
	if err != nil {
		return nil, xerrors.Errorf("failed to create tmpfile: %w", err)
	}
	filename := tmpfile.Name()

	defer tmpfile.Close()
	defer os.Remove(filename)

	result, err := exec.CommandContext(ctx, "trivy","image", "--skip-update", "--no-progress", "-o", filename, "-f", "json", image).CombinedOutput()
	if err != nil {
		i := strings.Index(string(result), "error in image scan")
		if i == -1 {
			return nil, xerrors.Errorf("failed to execute trivy: %w", err)
		} else {
			return nil, xerrors.Errorf("failed to execute trivy: %s", result[i:len(result)-1])
		}
	}
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, xerrors.Errorf("failed to read tmpfile: %w", err)
	}
	return body, nil
}

type TrivyVulnerability struct {
	VulnerabilityID  string   `json:"VulnerabilityID"`
	PkgName          string   `json:"PkgName"`
	InstalledVersion string   `json:"InstalledVersion"`
	FixedVersion     string   `json:"FixedVersion"`
	Title            string   `json:"Title"`
	Description      string   `json:"Description"`
	Severity         string   `json:"Severity"`
	References       []string `json:"References"`
}
"""
