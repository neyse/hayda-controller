from flask import Flask, request, jsonify

import trivy

app = Flask(__name__)

SHOW_VULN_LIST = False

# Function to respond back to the Admission Controller
def k8s_response(allowed, uid, message):
    return jsonify(
        {
            "apiVersion": "admission.k8s.io/v1",
            "kind": "AdmissionReview",
            "response": {
                "allowed": allowed,
                "uid": uid,
                "status": {"message": message},
            },
        }
    )


@app.route("/validate", methods=["POST"])
def deployment_webhook():
    request_info = request.get_json()
    # app.logger.info(request_info)
    uid = request_info["request"].get("uid")

    trivy.update_database()

    try:
        # Pod
        req_resource_type = request_info["request"]["resource"]["resource"]

        if req_resource_type == "pods":
            image = request_info["request"]["object"]["spec"]["containers"][0]["image"]
        elif req_resource_type == "deployments":
            image = request_info["request"]["object"]["spec"]["template"]["spec"][
                "containers"
            ][0]["image"]
        else:
            return k8s_response(
                False,
                uid,
                "We dont support this request resource type: {}".format(
                    req_resource_type
                ),
            )

        app.logger.info("Image: {}".format(image))
        data = trivy.scan(image, uid)

        vulnerabilities = data[0].get("Vulnerabilities", [])
        app.logger.info("Image: {} / Vulnerability Count: {}".format(image, len(vulnerabilities)))
        vuls_length = len(vulnerabilities)
        if vuls_length == 0:
            return k8s_response(True, uid, "Clean")
        else:
            if SHOW_VULN_LIST:
                app.logger.info(
                    """
                    Image: {}
                    Vulnerability list:
                    {}
                    """.format(image, data)
                )

            low_severity = 0
            medium_severity = 0
            high_severity = 0
            critical_severity = 0
            for vul in vulnerabilities:
                severity = vul.get("Severity")
                if severity == 'LOW':
                    low_severity += 1
                elif severity == 'MEDIUM':
                    medium_severity += 1
                elif severity == 'HIGH':
                    high_severity += 1
                elif severity == 'CRITICAL':
                    critical_severity += 1
                else:
                    app.logger.info("Unknown Severity: {}".format(severity))

            return k8s_response(False, uid, f"Your image '{image}' is not secure. Total Vulnerability Count: {vuls_length} (Critical: {critical_severity}, High: {high_severity}, Medium: {medium_severity}, Low: {low_severity})")
    except Exception as e:
        app.logger.error(e)
        return k8s_response(False, uid, "We have problem inside hayda-controller")
    finally:
        trivy.clear_cache()


if __name__ == "__main__":
    app.run(
        ssl_context=("certs/wardencrt.pem", "certs/wardenkey.pem"),
        debug=True,
        host="0.0.0.0",
    )
