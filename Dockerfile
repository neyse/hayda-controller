FROM python:3.8.10

RUN apt-get update -y \
    && apt-get install -y git curl

ENV TRIVY_VERSION 0.18.3

RUN curl -sSL https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz | tar -zx -C /tmp \
    && mv /tmp/trivy /usr/local/bin/trivy

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN python3 -m pip install -r requirements.txt

COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "app/hayda-controller.py" ]