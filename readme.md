# Hayda Controller

This project is for setting up a basic Kubernetes validating Admission
Controller using python.

Scan the images with trivy tool.

## Development

Steps to build your own admission controller.

1. Update `server.conf` to match your admission controller. You may need to update
   the service and namespace where the controller lives.

2. Run the certgen.sh script to create the self-signed SAN certificates for the
   admission controller.
   ```
   $ docker run -it --rm -v ${PWD}:/work -w /work debian bash
   $ ./certgen.sh
   ```

3. Get the base64 value of the ca.crt file created by the certgen.sh script. 
   ```
   $ cat certs/ca.crt | base64
   ```

4. Paste the base64 value into the caBundle location in the `.deploy/webhook.yaml` file.

5. Build the container using the Dockerfile within the directory. Push the image
   to your image repository
   ```
   $ docker image build -t ef9n/hayda-controller:v1.0.0 . 
   ```

6. Update the `.deploy/manifest.yaml` file to point to your new image.

7. Apply the `.deploy/manifest.yaml` file to deploy your admission controller within the
   cluster. And apply the `.deploy/webhook.yaml` file to deploy the validation configuration to the
   Kubernetes API server.
   ```
   $ kubectl apply -f .deploy
   ```

8. Test your app. If using the default warden.py included with this repository,
    there are three test manifests in the [test-pods](/test-pods) folder.
